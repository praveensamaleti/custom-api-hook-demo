const options = [
    {
        id: 0,
        value: 'Popular'
    },
    {
        id: 1,
        value: 'Upcoming'
    },
    {
        id: 2,
        value: 'Top Rated'
    },
    {
        id: 3,
        value: 'Now Showing'
    },
    
]

export {options}