
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';

import CardContent from '@material-ui/core/CardContent';

import Typography from '@material-ui/core/Typography';
import useAxios from './customHooks/useAxios';
import { Button } from '@material-ui/core';
import * as MoviesAPI from "./APIS/MoviesAPI.js"
import { useEffect, useState } from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { options } from './util';

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        maxWidth: 400,
        margin: 20
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

function Movies() {
    const [page, setPage] = useState(1);
    const [type, setType] = useState(0);

    const pmoviesAPI = useAxios(MoviesAPI.getPopularMovies, 1, "call");
    const umoviesAPI = useAxios(MoviesAPI.getUpcomingMovies, 1, "dont");
    const tmoviesAPI = useAxios(MoviesAPI.getTopRatedMovies, 1, "dont");
    const nmoviesAPI = useAxios(MoviesAPI.getNowPlayingMovies, 1, "dont");
    // const lmoviesAPI = useAxios(MoviesAPI.getLatestMovies, 1, "dont");
    const [movies, setMovies] = useState();
    const classes = useStyles();
    function handleNext() {
        if (type !== "4") {
            let newPage = page + 1;
            setPage(newPage)
            hitAPI(type)
        }
    }
    function handleChange(e) {
        setType(e.target.value)
        hitAPI(e.target.value)
    }
    useEffect(() => {
        switch (type) {
            case "0":
                setMovies(pmoviesAPI.response && pmoviesAPI.response.results)
                break;
            case "1":
                setMovies(umoviesAPI.response && umoviesAPI.response.results)
                break;
            case "2":
                setMovies(tmoviesAPI.response && tmoviesAPI.response.results)
                break;
            case "3":
                setMovies(nmoviesAPI.response && nmoviesAPI.response.results)
                break;
            default:
                setMovies(pmoviesAPI.response && pmoviesAPI.response.results)
        }
    }, [pmoviesAPI, umoviesAPI, tmoviesAPI, nmoviesAPI])
    function hitAPI(type) {
        switch (type) {
            case "0":
                pmoviesAPI.callAPI(page)
                break;
            case "1":
                umoviesAPI.callAPI(page)
                break;
            case "2":
                tmoviesAPI.callAPI(page)
                break;
            case "3":
                nmoviesAPI.callAPI(page)
                break;
            default:
                pmoviesAPI.callAPI(page)
        }
    }
    return (
        <>
            <div>Some</div>
            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel htmlFor="outlined-age-native-simple">Movie Type</InputLabel>
                <Select
                    native
                    value={type}
                    onChange={handleChange}
                    label="Movie Type"
                    inputProps={{
                        name: 'type',
                        id: 'outlined-age-native-simple',
                    }}
                >
                    {options.map((option, index) =>
                        <option value={option.id} key={index}>{option.value}</option>)}
                </Select>
            </FormControl>
            <Button onClick={handleNext}>Next</Button>
            <Box
                display="flex"
                flexWrap="wrap"
                p={1}
                m={1}
                bgcolor="background.paper"
            >
                {movies && movies.map((movie, index) =>
                    <Card className={classes.root} key={index} elevation={10}>
                        <CardContent>
                            <Typography className={classes.title} color="textSecondary" gutterBottom>
                                {movie.vote_average}
                            </Typography>
                            <Typography variant="h5" component="h2">
                                {movie.original_title}
                            </Typography>
                            <Typography className={classes.pos} color="textSecondary">
                                {movie.release_date}
                            </Typography>
                            <Typography variant="body2" component="p">
                                {movie.overview}
                            </Typography>
                        </CardContent>
                    </Card>
                )}
            </Box>
        </>
    )
}
export default Movies;