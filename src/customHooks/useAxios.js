import { useEffect, useState } from "react";

const useAxios = (apiMethod,...args) => {
    const [response, setResponse] = useState();
    const [loading, setLoading] = useState();
    const [error, setError] = useState();

    const callAPI = async (...args) => {
        setLoading(true)
        const response = await apiMethod(...args);
        setLoading(false)
        setError(!response.ok)
        setResponse(response.data)
        return response.data
    }

    useEffect(() => {
        args[1] === "call" && callAPI(...args)
    }, [])

    return { response, loading, error, callAPI }
}

export default useAxios;