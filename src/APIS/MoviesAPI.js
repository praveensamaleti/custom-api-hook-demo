import axios from "axios";

function getPopularMovies(...args) {
    const url = `https://api.themoviedb.org/3/movie/popular?api_key=07a7f693a056feaeb1004f6d09a98bcc&language=en-US&page=${args[0]}`
    return axios.get(url)
}

function getUpcomingMovies(...args) {
    const url = `https://api.themoviedb.org/3/movie/upcoming?api_key=07a7f693a056feaeb1004f6d09a98bcc&language=en-US&page=${args[0]}`
    return axios.get(url)
}

function getTopRatedMovies(...args) {
    const url = `https://api.themoviedb.org/3/movie/top_rated?api_key=07a7f693a056feaeb1004f6d09a98bcc&language=en-US&page=${args[0]}`
    return axios.get(url)
}

function getNowPlayingMovies(...args) {
    const url = `https://api.themoviedb.org/3/movie/now_playing?api_key=07a7f693a056feaeb1004f6d09a98bcc&language=en-US&page=${args[0]}`
    return axios.get(url)
}

function getLatestMovies(...args) {
    const url = `https://api.themoviedb.org/3/movie/latest?api_key=07a7f693a056feaeb1004f6d09a98bcc&language=en-US`
    return axios.get(url)
}

export { getPopularMovies, getUpcomingMovies, getTopRatedMovies, getNowPlayingMovies, getLatestMovies }